<?php

namespace Drupal\weta_tvss\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_tvss\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ChannelMappingsForm.
 *
 * @ingroup weta_tvss
 */
class ChannelMappingsForm extends ConfigFormBase {

  /**
   * TVSS API client.
   *
   * @var \Drupal\weta_tvss\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new WetaTvssSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\weta_tvss\ApiClient $api_client
   *   TVSS API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ChannelMappingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_tvss.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'weta_tvss_channel_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_tvss.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('weta_tvss.settings');
    $channel_vocabulary = $config->get('channels.channel_vocabulary');


    $form['channels_vocabulary_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Channels vocabulary'),
      '#open' => TRUE,
    ];
    $drupal_vocabularies = array_keys($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
    $vocabulary_options = array_combine($drupal_vocabularies, $drupal_vocabularies);
    $form['channels_vocabulary_type']['channel_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal channels vocabulary'),
      '#description' => $this->t('Select the Drupal vocabulary to use for TVSS channel content.'),
      '#default_value' => $config->get('channels.channel_vocabulary'),
      '#options' => $vocabulary_options,
    ];

    $form['channel_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Channel field mappings'),
      '#open' => FALSE,
    ];

    if (!empty($config->get('channels.channel_vocabulary'))) {
      $channel_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('taxonomy_term', $channel_vocabulary));
      $channel_field_options = ['unused' => 'unused'] +
        array_combine($channel_fields, $channel_fields);
      ;

      $pbs_channel_fields = $config->get('channels.channel_field_mappings');
      foreach (array_keys($pbs_channel_fields) as $field_name) {
        $form['channel_field_mappings'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $channel_field_options,
          '#required' => TRUE,
          '#default_value' => $pbs_channel_fields[$field_name],
        ];
      }
    }
    else {
      $form['channel_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal channel vocabulary (above) to choose field mappings.',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('weta_tvss.settings');

    $config->set('channels.channel_vocabulary', $values['channel_vocabulary']);

    // Channel field mappings.
    $pbs_channel_fields = $config->get('channels.channel_field_mappings');
    foreach (array_keys($pbs_channel_fields) as $field_name) {
      if (isset($values[$field_name])) {
        $config->set('channels.channel_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }
}
