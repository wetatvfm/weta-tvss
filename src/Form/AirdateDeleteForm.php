<?php

namespace Drupal\weta_tvss\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Airdate entities.
 *
 * @ingroup weta_tvss
 */
class AirdateDeleteForm extends ContentEntityDeleteForm {


}
