<?php


namespace Drupal\weta_tvss\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_tvss\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AirdateMappingsForm.
 *
 * @ingroup weta_tvss
 */
class AirdateMappingsForm extends ConfigFormBase {

  /**
   * TVSS API client.
   *
   * @var \Drupal\weta_tvss\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new WetaTvssSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\weta_tvss\ApiClient $api_client
   *   TVSS API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigFormBase|AirdateMappingsForm|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_tvss.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'weta_tvss_airdate_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_tvss.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('weta_tvss.settings');
    $airdate_entity = $config->get('airdates.airdates_entity');


    $form['airdates_entity_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Airdates entity'),
      '#open' => TRUE,
    ];

    $form['airdates_entity_type']['airdate_entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal airdates entity'),
      '#description' => $this->t('The Drupal entity to use for TVSS airdate content.'),
      '#default_value' => $airdate_entity,
      '#attributes' => ['readonly' => 'readonly']
    ];

    $form['airdate_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Airdate field mappings'),
      '#open' => FALSE,
    ];

    if (!empty($config->get('airdates.airdate_entity'))) {
      $airdate_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('airdate', $airdate_entity));
      $airdate_field_options = ['unused' => 'unused'] +
        array_combine($airdate_fields, $airdate_fields);
      ;

      $pbs_airdate_fields = $config->get('airdates.airdate_field_mappings');
      foreach (array_keys($pbs_airdate_fields) as $field_name) {
        $form['airdate_field_mappings'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $airdate_field_options,
          '#required' => TRUE,
          '#default_value' => $pbs_airdate_fields[$field_name],
        ];
      }
    }
    else {
      $form['airdate_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal airdate entity (above) to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
      '#open' => FALSE,
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('airdates.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $pbs_image_fields = $config->get('airdates.image_field_mappings');
      foreach ($pbs_image_fields as $pbs_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$pbs_image_field] = [
          '#type' => 'select',
          '#title' => $pbs_image_field,
          '#options' => $image_field_options,
          '#default_value' => $field_value,
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }


    // Daypart configuration.
    $form['daypart_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Daypart settings'),
      '#open' => FALSE,
    ];

    $dayparts = $config->get('airdates.dayparts');

    // Set up select list options.
    $hour_options = ['Midnight'];
    $hour = 1;
    while ($hour < 24) {
      $suffix = ' AM';
      if ($hour >= 12) {
        $suffix = ' PM';
      }
      if ($hour > 12) {
        $hour_options[] = $hour - 12 . $suffix;
      }
      else {
        $hour_options[] = $hour . $suffix;
      }
      $hour++;
    }
    $hour_options[] = 'Midnight (next day)';
    $hour_settings = ['start', 'end'];

    foreach ($dayparts as $daypart => $daypart_settings) {
      $form['daypart_settings'][$daypart] = [
        '#type' => 'details',
        '#title' => $daypart,
        '#open' => TRUE,
      ];
      foreach (array_keys($daypart_settings) as $daypart_setting) {
        if (in_array($daypart_setting, $hour_settings)) {
          $form['daypart_settings'][$daypart][$daypart_setting] = [
            '#type' => 'select',
            '#title' => $daypart_setting,
            '#options' => $hour_options,
            '#default_value' => $dayparts[$daypart][$daypart_setting],
            '#required' => TRUE,
          ];
        }
        else {
          $form['daypart_settings'][$daypart][$daypart_setting] = [
            '#type' => 'textfield',
            '#title' => $daypart_setting,
            '#default_value' => $dayparts[$daypart][$daypart_setting],
            '#required' => TRUE,
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('weta_tvss.settings');

    $config->set('airdates.airdate_entity', $values['airdate_entity']);

    // airdate field mappings.
    $pbs_airdate_fields = $config->get('airdates.airdate_field_mappings');
    foreach (array_keys($pbs_airdate_fields) as $field_name) {
      if (isset($values[$field_name])) {
        $config->set('airdates.airdate_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Image settings.
    $config->set('airdates.image_media_type', $values['image_media_type']);
    $pbs_image_fields = $config->get('airdates.image_field_mappings');
    foreach ($pbs_image_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('airdates.image_field_mappings.' . $field_name, $values[$field_name]);
      }
    }


    $config->save();

    parent::submitForm($form, $form_state);
  }

}
