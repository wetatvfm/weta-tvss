<?php

namespace Drupal\weta_tvss\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_tvss\ApiClient;
use Drupal\weta_tvss\ShowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ShowMappingsForm.
 *
 * @ingroup weta_tvss
 */
class ShowMappingsForm extends ConfigFormBase {

  /**
   * TVSS API client.
   *
   * @var \Drupal\weta_tvss\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new WetaTvssSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\weta_tvss\ApiClient $api_client
   *   TVSS API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ShowMappingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_tvss.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'weta_tvss_show_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_tvss.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('weta_tvss.settings');
    $show_content_type = $config->get('shows.drupal_show_content');

    $form['shows_queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Show queue settings'),
      '#open' => TRUE,
    ];

    $form['shows_queue']['shows_queue_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable automated queue building'),
      '#description' => $this->t('Enable incremental updates to local Show nodes from TVSS data.'),
      '#default_value' => $config->get(ShowManager::getAutoUpdateConfigName()),
      '#return_value' => TRUE,
    ];

    $interval_options = [1, 900, 3600, 10800, 21600, 43200, 86400, 604800];
    $form['shows_queue']['shows_queue_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Queue builder update interval'),
      '#description' => $this->t('How often to check the TVSS for new or updated shows to add to the queue. The queue itself is processed once every cron ron (or by an external cron operation).'),
      '#default_value' => $config
        ->get(ShowManager::getAutoUpdateIntervalConfigName()),
      '#options' => array_map(
        [$this->dateFormatter, 'formatInterval'],
        array_combine($interval_options, $interval_options)
      ),
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['show_content_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Show content type'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $drupal_content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    $content_type_options = array_combine($drupal_content_types, $drupal_content_types);
    $form['show_content_type']['drupal_show_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal show content type'),
      '#description' => $this->t('Select the Drupal content type to use for TVSS show content.'),
      '#default_value' => $config->get('shows.drupal_show_content'),
      '#options' => $content_type_options,
    ];

    $form['show_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Show field mappings'),
      '#description' => $this->t('TVSS Only fields are used only by the TVSS API. Shared fields are also used by the PBS Media Manager API. In case of a conflict, the PBS Media Manager data is used.'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (!empty($config->get('shows.drupal_show_content'))) {
      $show_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('node', $show_content_type));
      $show_field_options = ['unused' => 'unused'] +
        array_combine($show_fields, $show_fields);

      $form['show_field_mappings']['tvss_only'] = [
        '#type' => 'details',
        '#title' => $this->t('TVSS Only'),
        '#open' => TRUE,
      ];

      $tvss_only_fields = $config->get('shows.mappings.tvss_only');
      foreach (array_keys($tvss_only_fields) as $field_name) {
        $form['show_field_mappings']['tvss_only'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $show_field_options,
          '#required' => TRUE,
          '#default_value' => $tvss_only_fields[$field_name],
        ];
      }

      $form['show_field_mappings']['shared'] = [
        '#type' => 'details',
        '#title' => $this->t('Shared'),
        '#open' => TRUE,
      ];

      $shared_fields = $config->get('shows.mappings.shared');
      foreach (array_keys($shared_fields) as $field_name) {
        $form['show_field_mappings']['shared'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $show_field_options,
          '#required' => TRUE,
          '#default_value' => $shared_fields[$field_name],
        ];
      }
    }
    else {
      $form['show_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal show content type (above) to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('shows.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $pbs_image_fields = $config->get('shows.image_field_mappings');
      foreach ($pbs_image_fields as $pbs_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$pbs_image_field] = [
          '#type' => 'select',
          '#title' => $pbs_image_field,
          '#options' => $image_field_options,
          '#default_value' => $field_value,
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('weta_tvss.settings');

    $config->set('shows.drupal_show_content', $values['drupal_show_content']);

    // Queue settings.
    $config->set(
      ShowManager::getAutoUpdateConfigName(),
      $values['shows_queue_enable']
    );
    if ($values['shows_queue_enable']) {
      $config->set(
        ShowManager::getAutoUpdateIntervalConfigName(),
        (int) $values['shows_queue_interval']
      );
    }

    // Show field mappings.
    $pbs_show_fields = $config->get('shows.mappings');
    foreach (array_keys($pbs_show_fields) as $field_name) {
      if (isset($values[$field_name])) {
        $config->set('shows.mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Image settings.
    $config->set('shows.image_media_type', $values['image_media_type']);
    $pbs_image_fields = $config->get('shows.image_field_mappings');
    foreach ($pbs_image_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('shows.image_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
