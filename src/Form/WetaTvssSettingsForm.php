<?php

namespace Drupal\weta_tvss\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_tvss\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WetaTvssSettingsForm.
 *
 * @ingroup weta_tvss
 */
class WetaTvssSettingsForm extends ConfigFormBase {

  /**
   * WETA TVSS API client.
   *
   * @var \Drupal\weta_tvss\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new WetaTvssSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\weta_tvss\ApiClient $api_client
   *   WETA TVSS API client.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): WetaTvssSettingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_tvss.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'weta_tvss_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_tvss.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state):
  array {
    $config = $this->config('weta_tvss.settings');

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('PBS TVSS API settings'),
      '#open' => TRUE,
    ];

    $form['api']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('TVSS API key.'),
      '#default_value' => $this->apiClient->getApiKey(),
      '#disabled' => $this->configIsOverridden(ApiClient::CONFIG_KEY),
    ];

    $form['api']['call_sign'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API call sign'),
      '#description' => $this->t('TVSS API station call sign.'),
      '#default_value' => $this->apiClient->getCallSign(),
      '#disabled' => $this->configIsOverridden(ApiClient::CONFIG_CALL_SIGN),
    ];

    // Create an array of all Drupal users.
    $users = $this->entityTypeManager->getStorage('user')->loadMultiple();
    $all_users = [];
    foreach ($users as $user) {
      $all_users[$user->id()] = $user->getDisplayName();
    }
    unset($all_users[0]);
    asort($all_users);
    $form['api']['queue_user'] = [
      '#type' => 'select',
      '#title' => 'Default author of TVSS content',
      '#default_value' => $config->get('api.queue_user'),
      '#options' => $all_users,
    ];

    $form['api']['disable_notices'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable notices'),
      '#default_value' => $config->get('api.disable_notices'),
      '#description' => $this->t('If checked, notices that items have been created, updated, skipped, or ignored will not be logged. Errors and warnings will still be recorded.'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('weta_tvss.settings');

    if (!$this->configIsOverridden(ApiClient::CONFIG_KEY)) {
      $config->set(ApiClient::CONFIG_KEY, $form_state->getValue('key'));
    }

    if (!$this->configIsOverridden(ApiClient::CONFIG_CALL_SIGN)) {
      $config->set(
        ApiClient::CONFIG_CALL_SIGN,
        $form_state->getValue('call_sign')
      );
    }

    $config->set('api.queue_user', $form_state->getValue('queue_user'));
    $config->set('api.disable_notices',  $form_state->getValue('disable_notices'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Check if a config variable is overridden by local settings.
   *
   * @param string $name
   *   Settings key to check.
   *
   * @return bool
   *   TRUE if the config is overwritten, FALSE otherwise.
   */
  protected function configIsOverridden(string $name): bool {
    $original = $this->configFactory
      ->getEditable('weta_tvss.settings')
      ->get($name);
    $current = $this->configFactory
      ->get('weta_tvss.settings')
      ->get($name);
    return $original != $current;
  }

}
