<?php


namespace Drupal\weta_tvss;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\weta_tvss\Entity\Airdate;
use Drupal\weta_tvss\Entity\AirdateInterface;
use Exception;
use stdClass;

class AirdateManager extends ApiContentManagerBase {
  /**
   * State key for the last time the Schedule Pruner queue was updated.
   */
  const LAST_PRUNE_KEY = 'weta_tvss.last_prune';
  
  /**
   * State key for the last time schedule items were fully synced.
   */
  const LAST_FULL_UPDATE = 'weta_tvss.last_full_update';
  
  /**
   * State key for the last time the current day's schedule items were synced.
   */
  const LAST_DAY_UPDATE = 'weta_tvss.last_day_update';
  
  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'weta_tvss.shows.last_update';
  
  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'airdate';
  }
  
  /**
   * @inheritDoc
   */
  public function getBundleId(): string {
    return '';
  }
  
  /**
   * @inheritDoc
   */
  public static function getQueueName(): string {
    return 'weta_tvss.queue.airdates_pruner';
  }
  
  /**
   * @inheritDoc
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }
  
  /**
   * @inheritDoc
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }
  
  /**
   * @inheritDoc
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }
  
  /**
   * @inheritDoc
   */
  public static function getAutoUpdateConfigName(): string {
    return 'airdates.queue.autoupdate';
  }
  
  /**
   * @inheritDoc
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'shows.queue.autoupdate_interval';
  }
  
  /**
   * @inheritDoc
   */
  public function updateQueue(DateTime $since = NULL): bool {
    // Get all expired airdates and add them to the queue, in groups of 50.
    $airdates = $this->queueOldAirdatesForDelete();
    if (!empty($airdates)) {
      foreach (array_chunk($airdates, 50) as $airdate_group) {
        $this->getQueue()->createItem($airdate_group);
      }
    }
    return TRUE;
  }
  
  /**
   * Gets the datetime of the last pruning pass to delete old schedule items.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   Datetime of last run or yesterday if nothing found in state.
   *
   * @throws \Exception
   */
  public function getLastPruneTime(): DrupalDateTime {
    $last_update = $this->state->get(self::LAST_PRUNE_KEY);
    if (empty($last_update)) {
      $last_update = new DrupalDateTime('yesterday');
    }
    return $last_update;
  }
  
  /**
   * Sets the last schedule item pruning pass in state.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $time
   *   Datetime to update with.
   *
   * @see \Drupal\weta_tvss\ScheduleItemManager::getLastPruneTime()
   */
  public function setLastPruneTime(DrupalDateTime $time): void {
    $this->state->set(self::LAST_PRUNE_KEY, $time);
  }
  
  /**
   * Gets the datetime of the last airdate update for the current day.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   Datetime of last run or yesterday if nothing found in state.
   *
   * @throws \Exception
   */
  public function getLastDayUpdate(): DrupalDateTime {
    $last_update = $this->state->get(self::LAST_DAY_UPDATE);
    if (empty($last_update)) {
      $last_update = new DrupalDateTime('yesterday');
    }
    return $last_update;
  }
  
  /**
   * Sets the datetime of last airdate update for the current day.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $time
   *   Datetime to update with.
   *
   * @see \Drupal\weta_tvss\AirdateManager::getLastDayUpdate()
   */
  public function setLastDayUpdate(DrupalDateTime $time): void {
    $this->state->set(self::LAST_DAY_UPDATE, $time);
  }
  
  /**
   * Gets the datetime of the last full airdate update.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   Datetime of last run or yesterday if nothing found in state.
   *
   * @throws \Exception
   */
  public function getLastFullUpdate(): DrupalDateTime {
    $last_update = $this->state->get(self::LAST_FULL_UPDATE);
    if (empty($last_update)) {
      $last_update = new DrupalDateTime('yesterday');
    }
    return $last_update;
  }
  
  /**
   * Sets the datetime of the last full airdate update.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $time
   *   Datetime to update with.
   *
   * @see \Drupal\weta_tvss\ScheduleItemManager::getLastFullUpdate()
   */
  public function setLastFullUpdate(DrupalDateTime $time): void {
    $this->state->set(self::LAST_FULL_UPDATE, $time);
  }
  
  
  /**
   * Updates as much as possible from the TVSS from a date forward.
   *
   * The TVSS API generally provides about two weeks of future listings, but
   * this method will churn until it reaches a date with no listing data.
   *
   * This process can take some time, so it is recommended that this method be
   * run in an environment with no PHP execution timeout.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   A DrupalDateTime is used here as a means to (sort of) ensure that the
   *   method is dealing with a proper timezone. Defaults to today.
   *
   * @throws \Exception
   */
  public function updateFromDate(DrupalDateTime $date = NULL): void {
    if (empty($date)) {
      $date = new DrupalDateTime();
    }
    
    while ($this->updateByDate($date->format('Y-m-d'))) {
      $date->add(new DateInterval('P1D'));
    }
  }
  
  /**
   * Update all airdate nodes from the TVSS API for a date.
   *
   * This method will:
   *  - Add any newly discovered API items as airdates.
   *  - Update any matching existing airdates found in the API. The API
   *    does not provided any method for recognizing changes, so all nodes will
   *    be updated.
   *  - Remove any airdates _not_ found in the API.
   *
   * @param string $date
   *   Date string in the format Y-m-d. This is provided as a string in order to
   *   support Channel-based timezones for listing data.
   *
   * @return bool
   *   TRUE if at least one listing is found and updated, FALSE otherwise.
   */
  public function updateByDate(string $date): bool {
    $date_utc = DateTime::createFromFormat('Y-m-d', $date);
    $feeds = $this->client->getListings($date_utc, FALSE, TRUE);
    $disable_notices = $this->config->get('api.disable_notices');
    
    // This variable will be used as the return value. It will be flipped to
    // TRUE as long as least one listing is found and processed.
    $updated = FALSE;
    
    // Keep track of the number of airdates updated.
    $count = 0;
    
    foreach ($feeds as $feed) {
      
      try {
        $channelManager = \Drupal::service('weta_tvss.channel_manager');
        /** @var \Drupal\taxonomy\TermInterface $channel */
        $channel = $channelManager->addOrUpdateChannel($feed);
      }
      catch (Exception $e) {
        $this->logger->critical($this->t('Unable to add/update Channel
          taxonomy content for TVSS feed @cid due to exception. See site log for
          details.', ['@cid' => $feed->cid]));
        watchdog_exception('weta_tvss', $e);
        continue;
      }
      
      // Match $date's timezone to the Channel. This will help with time
      // calculations during listing update handling.
      $timezone_field = $this->config->get('channels.channel_field_mappings.timezone');
      if (!$channel->get($timezone_field)->isEmpty()) {
        $timezone = new DateTimeZone($channel->get($timezone_field)->value);
      }
      else {
        $this->logger->critical($this->t('Could not determine timezone for
          Channel {@label} (@id). Listing import abandoned!', [
          '@label' => $channel->label(),
          '@id' => $channel->id(),
        ]));
        continue;
      }
      
      // Create a date object in the Channel's timezone to use during listing
      // processing.
      $listing_date = DateTimeImmutable::createFromFormat(
        'Y-m-d H:i',
        "$date 00:00",
        $timezone
      );
      
      $cids = [];
      foreach ($feed->listings as $listing) {
        $listing->channel = $channel->id();
        $listing->date = $listing_date;
        
        try {
          $this->addOrUpdateContent($listing);
        }
        catch (Exception $e) {
          $this->logger->critical($this->t('Unable to add/update Airdate
          content for TVSS listing @cid due to exception. See site log for
          details.', ['@cid' => $listing->cid]));
          watchdog_exception('weta_tvss', $e);
          // This is allowed to fall through because the CID should still be
          // recorded to prevent accidentally deleting nodes during unmatched
          // node processing below.
        }
        
        // Capture all CIDs for unmatched processing (below).
        $cids[] = $listing->cid;
        
        // Indicates that at least one listing was found and processed.
        $updated = TRUE;
        
        // Increment the number of airdates processed.
        $count++;
      }
      
      $this->removeUnmatchedContent($listing_date, $channel, $cids);
    }
    
    if (!$disable_notices) {
      $this->logger->notice($this->t('@count airdates for @date have been updated.', [
        '@count' => $count,
        '@date' => $date
      ]));
    }
    
    return $updated;
  }
  
  /**
   * Deletes any nodes with TVSS API CIDs not found in the provided $cids array.
   *
   * This is necessary because it is possible for an entry to be deleted and
   * replaced with something in the TVSS API. When this happens, the old nodes
   * need to be removed in order to prevent time overlaps in a schedule.
   *
   * @param \DateTimeImmutable $date
   *   Start date to use for query conditions.
   * @param \Drupal\taxonomy\TermInterface $channel
   *   Channel to use for query conditions.
   * @param array $expected_cids
   *   TVSS CIDs that should be found from the query.
   */
  public function removeUnmatchedContent(
    DateTimeImmutable $date,
    TermInterface $channel,
    array $expected_cids
  ): void {
    $airdates = $this->getAirdatesForDateAndChannel($date, $channel);
    
    
    // Get a list of "unmatched" airdates to be deleted.
    if (!empty($airdates)) {
      $unmatched_airdates = array_udiff($airdates, $expected_cids, function ($a, $b) {
        $cid_field_name = $this->config->get('airdates.airdate_field_mappings.cid');
        // Inputs are _not_ guaranteed to have a specific type.
        if ($a instanceof AirdateInterface) {
          $a = $a->get($cid_field_name)->value;
        }
        if ($b instanceof AirdateInterface) {
          $b = $b->get($cid_field_name)->value;
        }
        
        return $a <=> $b;
      });
    }
    
    if (!empty($unmatched_airdates)) {
      try {
        $storage = $this->getStorage();
        $storage->delete($unmatched_airdates);
      }
      catch (Exception $e) {
        watchdog_exception('weta_tvss', $e);
        return;
      }
    }
  }
  
  /**
   * Add or update airdate content.
   *
   * @param stdClass $item
   *   An airdate item.
   *
   * @return ContentEntityInterface
   *   An airdate entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addOrUpdateContent(stdClass $item): ContentEntityInterface {
    
    $airdate = $this->getContentByCid($item->cid);
    if (empty($airdate)) {
      $airdate = Airdate::create();
      $airdate->enforceIsNew();
    }
    
    $mappings = $this->config->get('airdates.airdate_field_mappings');
    
    $airdate->set($mappings['cid'], $item->cid);
    $airdate->setName($item->title);
    
    if (isset($item->episode_title) && !empty($item->episode_title)) {
      $airdate->set($mappings['episode_title'], $item->episode_title);
    }
    
    if (isset($item->episode_description)
      && !empty($item->episode_description)) {
      $airdate->set($mappings['description'], $item->episode_description);
    }
    else {
      $airdate->set($mappings['description'], $item->description);
    }
    
    $airdate->set($mappings['channel'], $item->channel);
    
    // Handle start date/time.
    $start_date = self::createStorageDate($item->date, $item->start_time);
    $airdate->set(
      $mappings['start_time'],
      $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );
    
    // Handle day part
    $dayparts = $this->config->get('airdates.dayparts');
    // Remove the last two digits of the start time to get the hour in military.
    $start_hour = substr($item->start_time, 0, -2);
    foreach ($dayparts as $delta => $daypart_settings) {
      if ($start_hour >= $daypart_settings['start'] && $start_hour <= $daypart_settings['end']) {
        $airdate->set($mappings['day_part'], $delta);
      }
    }
    
    $airdate->set($mappings['date'], $item->date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT));
    $airdate->set($mappings['minutes'], $item->minutes);
    $airdate->set($mappings['duration'], $item->duration);
    
    if (isset($item->program_external_id)
      && !empty($item->program_external_id)) {
      $airdate->set($mappings['program_external_id'], $item->program_external_id);
      
    }
    
    if (isset($item->program_id) && !empty($item->program_id)) {
      $airdate->set($mappings['program_id'], $item->program_id);
    }
    
    if (isset($item->show_external_id)
      && !empty($item->show_external_id)) {
      $airdate->set($mappings['show_external_id'], $item->show_external_id);
      
    }
    
    if (isset($item->show_id) && !empty($item->show_id)) {
      $airdate->set($mappings['show_id'], $item->show_id);
    }
    
    $tvss_id = $this->getIndex($item);
    $airdate->set($mappings['tvss_id'], $tvss_id);
    
    // Check to see if this airdate already has a show attached.
    // If not, create it using the current airdate.
    // If this is of type "customshow" there will be no show.
    $showManager = \Drupal::service('weta_tvss.show_manager');
    $show = $showManager->getShowNodeByTvssId($tvss_id);
    if (empty($show) && $item->type != 'customshow') {
      $item->tvss_id = $tvss_id;
      $showManager->addOrUpdateShow($item);
      $show = $showManager->getShowNodeByTvssId($tvss_id);
    }
    if ($show) {
      $airdate->set($mappings['show'], $show->id());
    }
    else {
      $airdate->set($mappings['show'], NULL);
    }
    
    
    
    if ($uid = $this->config->get('api.queue_user')) {
      $airdate->setOwnerId($uid);
    }
    
    // Process images.
    if (!empty($mappings['tvss_image']) && $image_field = $mappings['tvss_image']) {
      if ($image_field != 'unused') {
        $this_image = NULL;
        $image_title = NULL;
        if (isset($item->episode_images) && !empty($item->episode_images)) {
          $this_image = $this->selectImage($item->episode_images);
          if ($this_image) {
            $this_image->type = 'episode';
            $image_title = $item->title . ": " . $item->episode_title . ": TVSS: " .
              $this_image->external_profile;
          }
        }
        elseif (isset($item->images) && !empty($item->images)) {
          $this_image = $this->selectImage($item->images);
          if ($this_image) {
            $this_image->type = 'show';
          }
          $image_title = $item->title . ": TVSS: " .
            $this_image->external_profile;
        }
        
        if ($this_image) {
          $this_image->airdate_id = $airdate->id();
          // Check to see if this item has an image already.
          $media_image = NULL;
          if ($airdate->get($image_field)->target_id) {
            // Get the existing image entity.
            $image_id = $airdate->get($image_field)->target_id;
            $media_storage = $this->entityTypeManager->getStorage('media');
            $media_image = $media_storage->load($image_id);
          }
          if ($media_id = $this->addOrUpdateMediaImage(
            $this_image,
            $image_title,
            'airdate',
            $media_image
          )) {
            $airdate->set($image_field, ['target_id' => $media_id]);
          }
        }
      }
    }
    
    $airdate->save();
    
    return $airdate;
  }
  
  /**
   * Select the first appropriate image result.
   *
   * @param object $images
   *
   * @return object | void
   */
  private function selectImage($images) {
    $this_image = NULL;
    foreach ($images as $image) {
      // Filter returned images to find one that matches
      // the correct (16:9) ratio in order of preference.
      
      if ($image->ratio == '16:9') {
        switch ($image->external_profile) {
          case 'Iconic':
            $this_image = $image;
            break;
          case 'Banner':
            $this_image = $image;
            break;
          case 'Banner-L1':
            $this_image = $image;
            break;
          case 'Banner-L2':
            $this_image = $image;
            break;
          case 'Banner-L3':
            $this_image = $image;
            break;
          case 'Banner-LO':
            $this_image = $image;
            break;
          case 'Logo':
            $this_image = $image;
            break;
          case 'Staple':
            $this_image = $image;
            break;
          default:
            $this_image = $image;
        }
      }
    }
    
    return $this_image;
    
  }
  
  /**
   * Gets a storage-ready DateTime object for a date and time.
   *
   * @param \DateTimeImmutable $date
   *   Listing's run date in a Channel's timezone.
   * @param string $time
   *   Time in the format HHMM.
   *
   * @return \DateTimeImmutable
   *   The Listing's start date and time in the storage TZ and format.
   */
  private static function createStorageDate(
    DateTimeImmutable $date,
    string $time
  ): DateTimeImmutable {
    $h = (int) substr($time, 0, 2);
    $m = (int) substr($time, 2);
    $storage_tz = new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
    return $date->setTime($h, $m)->setTimeZone($storage_tz);
  }
  
  /**
   * Gets a specific listing for a date by CID.
   *
   * @param \DateTime $date
   *   Date to check.
   * @param string $cid
   *   CID to search for.
   *
   * @return object|null
   *   The listing item or NULL if the CID is not found.
   */
  public function getListing(DateTime $date, string $cid): ?stdClass {
    $feeds = $this->client->getListings($date);
    foreach ($feeds as $feed) {
      $key = array_search($cid, array_column($feed->listings, 'cid'));
      if ($key) {
        return $feed->listings[$key];
      }
    }
    return NULL;
  }
  
  /**
   * Gets all airdates for a given date and channel.
   *
   * @param \DateTimeImmutable $date
   *   Date to use for query.
   * @param \Drupal\taxonomy\TermInterface $channel
   *   Channel to use for query.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   *   Matching airdates or an empty array if an exception occurs.
   */
  public function getAirdatesForDateAndChannel(
    DateTimeImmutable $date,
    TermInterface $channel
  ): array {
    $airdates = [];
    
    try {
      $definition = $this->getDefinition();
      $storage = $this->getStorage();
    }
    catch (Exception $e) {
      watchdog_exception('weta_tvss', $e);
      return $airdates;
    }
    
    try {
      // Get all airdates with date $date and channel $channel.
      /** @var \DateTimeImmutable $start_time */
      $start_time = $date->setTime(0, 0)
        ->setTimezone(new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $end_time = $start_time->add(new DateInterval('P1D'));
    }
    catch (Exception $e) {
      watchdog_exception('weta_tvss', $e);
      return $airdates;
    }
    
    $mappings = $this->config->get('airdates.airdate_field_mappings');
    
    $query = $storage->getQuery();
    $airdate_ids = $query->condition($mappings['channel'] . '.target_id', $channel->id())
      ->condition(
        $mappings['start_time'],
        $start_time->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '>='
      )
      ->condition(
        $mappings['start_time'],
        $end_time->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '<='
      )
      ->accessCheck(FALSE)
      ->execute();
    
    return $storage->loadMultiple($airdate_ids);
  }
  
  /**
   * Gets airdates for a given show starting with today.
   
   * @param string $tvss_id
   *   TVSS ID to search for.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   *   Matching airdates or an empty array if an exception occurs.
   */
  public function getAirdatesForShow(string $tvss_id): array {
    $airdates = [];
    
    try {
      $definition = $this->getDefinition();
      $storage = $this->getStorage();
    }
    catch (Exception $e) {
      watchdog_exception('weta_tvss', $e);
      return $airdates;
    }
    
    try {
      $date = new DateTimeImmutable();
      $start_time = $date->setTime(0, 0)
        ->setTimezone(new DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    }
    catch (Exception $e) {
      watchdog_exception('weta_tvss', $e);
      return $airdates;
    }
    
    $mappings = $this->config->get('airdates.airdate_field_mappings');
    
    $query = $storage->getQuery();
    $airdate_ids = $query->condition($mappings['tvss_id'], $tvss_id)
      ->condition(
        $mappings['start_time'],
        $start_time->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '>='
      )
      ->accessCheck(FALSE)
      ->execute();
    
    return $storage->loadMultiple($airdate_ids);
  }
  
  /**
   * Queues items older than a certain age to be deleted.
   *
   * @param string $interval_spec
   *   An interval spec for DateInterval. Any nodes older than than this will be
   *   queued for delete. Defaults to two weeks (P14D).
   *
   * @return array|null
   *
   * @see \DateInterval::__construct
   */
  public function queueOldAirdatesForDelete(string $interval_spec = 'P14D'):
  array|null {
    try {
      $date_limit = new DateTime();
      $date_limit->sub(new DateInterval($interval_spec));
    }
    catch (Exception $e) {
      watchdog_exception('weta_tvss', $e);
      return NULL;
    }
    
    try {
      $storage = $this->getStorage();
    }
    catch (Exception $e) {
      watchdog_exception('weta_tvss', $e);
      return NULL;
    }
    
    // Get all Airdates older than $date_limit.
    $mappings = $this->config->get('airdates.airdate_field_mappings');
    
    $query = $storage->getQuery();
    $query->condition(
      $mappings['start_time'],
      $date_limit->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      '<='
    );
    $query->accessCheck(FALSE);
    $query->sort($mappings['start_time'], 'ASC');
    return $query->execute();
    
  }
}
