<?php

namespace Drupal\weta_tvss;

use DateInterval;
use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Exception;

/**
 * Class ShowManager.
 *
 * @package Drupal\weta_tvss
 */
class ShowManager extends ApiContentManagerBase {

  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'weta_tvss.shows.last_update';

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('shows.drupal_show_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'weta_tvss.queue.shows';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return 'shows.queue.autoupdate';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'shows.queue.autoupdate_interval';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();

    // Get all shows available and add them to the queue.
    $shows = $this->getShows();
    foreach ($shows as $show) {
      $this->getQueue()->createItem($show);
    }

    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }
  /**
   * Get a de-duped list of all shows currently in the tv schedule.
   *
   * @return array
   *   Returns an array of show objects.
   */
  public function getShows(): array {
    $shows = [];
    // Start with today.
    $date = new DateTime();
    // Cycle through 15 days.
    for ($days = 0; $days <= 14; $days++) {
      // Query the API for the day's listings with images.
      $response = $this->client->getListings($date, FALSE, TRUE);

      foreach ($response as $channel) {
        foreach ($channel->listings as $listing) {
          $index = $this->getIndex($listing);
          $listing->tvss_id = $index;
          $shows[$index] = $listing;
        }
      }
      // Set the date object to the next day.
      $date->add(new DateInterval('P1D'));
    }

    return $shows;
  }


  /**
   * Get Show nodes with optional properties.
   *
   * @param array $properties
   *   Properties to filter Show nodes.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Shows nodes.
   */
  public function getShowNodes(array $properties = []): array {
    try {
      $definition = $this->entityTypeManager->getDefinition(self::getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage(self::getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $this->getBundleId(),
      ] + $properties);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    return $nodes;
  }

  /**
   * Gets a Show node by TMS ID.
   *
   * @param string $id
   *   TMS ID to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Show node with TMS ID or NULL if none found.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getShowNodeByTmsId(string $id): ?NodeInterface {
    $node = NULL;
    if ($tms_id_field = $this->config->get('shows.mappings.shared.program_external_id')) {
      $nodes = $this->getShowNodes([$tms_id_field => $id]);

      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for Media Manager TMS ID/TVSS Program External ID {id}. Node IDs found: {nid_list}. Using node {nid}.', [
              'id' => $id,
              'nid_list' => implode(', ', array_keys($nodes)),
              'nid' => $node->id(),
            ]);
          $duplicate_field = $this->config->get('shows.mappings.shared.duplicate');
          foreach ($nodes as $duplicate) {
            $duplicate->set($duplicate_field, TRUE);
            $duplicate->save();
          }

        }
      }
    }
    else {
      $this->logger->error('Mapping the show Program External ID field is required.');
    }
    return $node;
  }

  /**
   * Gets a Show node by TVSS ID.
   *
   * @param string $id
   *   TMS ID to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Show node with TMS ID or NULL if none found.
   */
  public function getShowNodeByTvssId(string $id): ?NodeInterface {
    $node = NULL;
    if ($tvss_id_field = $this->config->get('shows.mappings.tvss_only.tvss_id')) {
      $nodes = $this->getShowNodes([$tvss_id_field => $id]);

      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for TVSS ID {id}. Node IDs found: {nid_list}. Using node {nid}.', [
            'id' => $id,
            'nid_list' => implode(', ', array_keys($nodes)),
            'nid' => $node->id(),
          ]);
        }
      }
    }
    else {
      $this->logger->error('Mapping the show TVSS ID field is required.');
    }
    return $node;
  }

  /**
   * Gets a Show node by title.
   *
   * @param string $title
   *   Title to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Show node with TMS ID or NULL if none found.
   */
  public function getShowNodeByTitle(string $title): ?NodeInterface {
    $node = NULL;
    if ($tvss_id_field = $this->config->get('shows.mappings.shared.title')) {
      $nodes = $this->getShowNodes([$tvss_id_field => $title]);

      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for title {title}. Node IDs found: {nid_list}. Using node {nid}.', [
            'title' => $title,
            'nid_list' => implode(', ', array_keys($nodes)),
            'nid' => $node->id(),
          ]);
        }
      }
    }
    else {
      $this->logger->error('Mapping the title field is required.');
    }
    return $node;
  }



  /**
   * Adds or updates a Show node based on API data.
   *
   * @param object $item
   *   An API response object for the Show.
   * @param bool $force
   *   Whether to "force" the update. If FALSE (default) and a current
   *   show content node is found with a matching or newer "last_updated"
   *   value, the existing node will not be updated. If TRUE, "last_updated" is
   *   ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateShow(object $item, bool $force = FALSE): void {
    $mappings = $this->config->get('shows.mappings');
    $disable_notices = $this->config->get('api.disable_notices');

    // Check to see if the node already exists by querying
    // the TMS ID.
    if (isset($item->program_external_id)) {
      $node = $this->getShowNodeByTmsId($item->program_external_id);
    }

    // If not, check to see if the node already exists by querying the TVSS ID.
    if (empty($node) && $item->tvss_id) {
      $node = $this->getShowNodeByTvssId($item->tvss_id);
    }

    // Masterpiece is a problem child, as always.
    if (empty($node) && strpos($item->title, 'on Masterpiece')) {
      $search_title = trim(str_replace('on Masterpiece', '', $item->title));
      $node = $this->getShowNodeByTitle($search_title);
    }

    // If still not, create it.
    if (empty($node)) {
      $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
      $node = Node::create([
        $definition->getKey('bundle') => $this->getBundleId(),
      ]);
      if ($uid = $this->config->get('api.queue_user')) {
        $node->setOwnerId($uid);
      }
      $node->enforceIsNew();
    }


    $operation = $this->getOperation($item, $node, $mappings, $force);

    // Do not proceed if mappings are missing.
    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and updated_at fields for shows.'));
      return;
    }


    // Set values from TVSS API response.
    if (!empty($item->tvss_id) && $mappings['tvss_only']['tvss_id'] != 'unused') {
      $node->set($mappings['tvss_only']['tvss_id'], $item->tvss_id);
    }

    if (!empty($item->program_id) && $mappings['tvss_only']['program_id'] != 'unused') {
      $node->set($mappings['tvss_only']['program_id'], $item->program_id);
    }
    if (!empty($item->show_id) && $mappings['tvss_only']['show_id'] != 'unused') {
      $node->set($mappings['tvss_only']['show_id'], $item->show_id);
    }

    // Because Media Manager has primacy over TVSS, only
    // update the following fields if there is no MM ID.
    if (!$this->nodeHasGuid($node)) {
      $node->setTitle($item->title);
      $node->set($mappings['shared']['sorting_title'], $this->adjustTitle
      ($item->title));

      if (!empty($item->program_external_id) && $mappings['shared']['program_external_id'] != 'unused') {
        $node->set($mappings['shared']['program_external_id'], $item->program_external_id);
      }
      if (!empty($item->description) &&
        $mappings['shared']['description'] != 'unused') {
        $node->set($mappings['shared']['description'], $item->description);
      }
      if (!empty($item->nola_root) && $mappings['shared']['nola_root'] != 'unused') {
        $node->set($mappings['shared']['nola_root'], $item->nola_root);
      }
    }

    // Set the updated time to now, since it's not in the response.
    $request_time = \Drupal::time()->getRequestTime();
    $updated_at = new DateTime("@{$request_time}");
    $node->set(
      $mappings['tvss_only']['updated_at'],
      $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );


    // Process images.
    if ($images = $item->images) {
      foreach ($images as $image) {
        if (!empty($mappings['tvss_only']['tvss_image']) && $image_field = $mappings['tvss_only']['tvss_image']) {
          if ($image_field != 'unused') {

            // Filter returned images to find ones that match
            // the correct profile and ratio.
            // We are only interested in "Iconic" images with a
            // 4:3 (preferred) or 16:9 aspect ratio.
            $this_image = NULL;
            if ($image->external_profile == 'Iconic') {
              if ($image->ratio == '4:3') {
                $this_image = $image;
              }
              elseif ($image->ratio == '16:9') {
                $this_image = $image;
              }
            }

            if ($this_image) {

              // Check to see if this item has an image already.
              $media_image = NULL;
              if ($node->get($image_field)->target_id) {
                // Get the existing image entity.
                $image_id = $node->get($image_field)->target_id;
                $media_storage = $this->entityTypeManager->getStorage('media');
                $media_image = $media_storage->load($image_id);
              }
              if ($media_id = $this->addOrUpdateMediaImage(
                $this_image,
                $item->title . ": TVSS: " . $image->external_profile,
                'show',
                $media_image
              )) {
                $node->set($image_field, ['target_id' => $media_id]);
              }
            }
          }
        }
      }
    }

    $node->save();

    if (!empty($node->getTitle()) & !empty($operation)) {
      if (!$disable_notices) {
        $this->logger->notice($this->t('Show @title has been @operation', [
          '@title' => $node->getTitle(),
          '@operation' => $operation,
        ]));
      }
    }
  }

  /**
   * Adjust titles to account for A, An, and The.
   */
  public function adjustTitle($title) {
    $title_length = strlen($title);

    if (str_starts_with($title, 'A ')) {
      $title = substr($title, 2, ($title_length - 2)) . ', A';
    }
    elseif (str_starts_with($title, 'An ')) {
      $title = substr($title, 3, ($title_length - 3)) . ', An';
    }
    elseif (str_starts_with($title, 'The ')) {
      $title = substr($title, 4, ($title_length - 4)) . ', The';
    }

    return $title;
  }
}
