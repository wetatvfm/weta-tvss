<?php

namespace Drupal\weta_tvss;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Airdate entities.
 *
 * @ingroup weta_tvss
 */
class AirdateListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('Airdate ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /* @var \Drupal\weta_tvss\Entity\Airdate $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.airdate.edit_form',
      ['airdate' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
