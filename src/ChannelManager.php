<?php

namespace Drupal\weta_tvss;

use DateTime;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Exception;

/**
 * Class ChannelManager.
 *
 * @package Drupal\weta_tvss
 */
class ChannelManager extends ApiContentManagerBase {

  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'weta_tvss.channels.last_update';

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'taxonomy_term';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('channels.channel_vocabulary');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'weta_tvss.queue.channels';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return 'channels.queue.autoupdate';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'channels.queue.autoupdate_interval';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();

    // Get all shows available and add them to the queue.
    $shows = $this->getChannels();
    foreach ($shows as $show) {
      $this->getQueue()->createItem($show);
    }

    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Get a list of channels from the API.
   *
   * @return array
   *   Returns an array of channel objects.
   */
  public function getChannels(): array {
    try {
      $channels = $this->client->getToday();
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $channels = [];
    }

    if (!empty($channels)) {
      foreach ($channels as &$channel) {
        // remove the listings.
        unset($channel->listings);
      }
    }

    return $channels;
  }

  /**
   * @param string $cid
   *
   * @return object|null
   */
  public function getChannelByCid(string $cid): ?object {
    $cid_field = $this->config->get('channels.channel_field_mappings.cid');

    $properties = [$cid_field => $cid];

    try {
      $items = $this->getContentByProperties($properties, NULL, 'ASC');
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $items = NULL;
    }

    $channel = NULL;
    if (!empty($items)) {
      $channel = reset($items);
      if (count($items) > 1) {
        $this->logger->error($this->t('Multiple items found for TVSS CID @cid. @label IDs found: @id_list. Using @tid.', [
          '@cid' => $cid,
          '@label' => $channel->getEntityType()->getLabel(),
          '@id_list' => implode(', ', array_keys($items)),
          '@tid' => $channel->id(),
        ]));
      }
    }

    return $channel;
  }

  /**
   * Adds or updates a Channel term based on API data.
   *
   * @param object $item
   *   An API response object for the Channel.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addOrUpdateChannel(object $item): TermInterface|null {

    $config = $this->config;
    $mappings = $config->get('channels.channel_field_mappings');
    $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
    $key = $definition->getKey('bundle');
    $vocabulary = $config->get('channels.channel_vocabulary');

    // Check to see if the channel already exists by querying
    // the Channel ID.
    $channel = NULL;
    if ($item->cid) {
      $channel = $this->getChannelByCid($item->cid);
    }

    // If not, create it.
    if (empty($channel)) {

      $channel = Term::create([
        $key => $vocabulary,
      ]);
    }

    // Set values from TVSS API response.
    $channel->setName($item->full_name);

    if (!empty($item->cid) && $mappings['cid'] != 'unused') {
      $channel->set($mappings['cid'], $item->cid);
    }

    if (!empty($item->short_name) && $mappings['short_name'] != 'unused') {
      $channel->set($mappings['short_name'], $item->short_name);
    }

    if (!empty($item->external_id) && $mappings['external_id'] != 'unused') {
      $channel->set($mappings['external_id'], $item->external_id);
    }

    if (!empty($item->timezone) && $mappings['timezone'] != 'unused') {
      $channel->set($mappings['timezone'], $item->timezone);
    }

    if (!empty($item->analog_channel) && $mappings['analog_channel'] != 'unused') {
      $channel->set($mappings['analog_channel'], $item->analog_channel);
    }

    if (!empty($item->digital_channel) && $mappings['digital_channel'] != 'unused') {
      $channel->set($mappings['digital_channel'], $item->digital_channel);
    }


    $channel->save();

    if (!empty($channel->getName()) & !empty($operation)) {
      $this->logger->notice($this->t('Channel @title has been @operation', [
        '@title' => $channel->getName(),
        '@operation' => $operation,
      ]));
    }

    return $channel;
  }
}
