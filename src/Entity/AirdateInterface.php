<?php

namespace Drupal\weta_tvss\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Airdate entities.
 *
 * @ingroup weta_tvss
 */
interface AirdateInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Airdate name.
   *
   * @return string
   *   Name of the Airdate.
   */
  public function getName(): string;

  /**
   * Sets the Airdate name.
   *
   * @param string $name
   *   The Airdate name.
   *
   * @return \Drupal\weta_tvss\Entity\AirdateInterface
   *   The called Airdate entity.
   */
  public function setName(string $name): AirdateInterface;

  /**
   * Gets the Airdate creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Airdate.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Airdate creation timestamp.
   *
   * @param int $timestamp
   *   The Airdate creation timestamp.
   *
   * @return \Drupal\weta_tvss\Entity\AirdateInterface
   *   The called Airdate entity.
   */
  public function setCreatedTime(int $timestamp): AirdateInterface;

}
