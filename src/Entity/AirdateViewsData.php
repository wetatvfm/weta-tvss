<?php

namespace Drupal\weta_tvss\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Airdate entities.
 */
class AirdateViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    // Additional information for Views integration, such as table joins, can be
    // put here.
    return parent::getViewsData();
  }

}
