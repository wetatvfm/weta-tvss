<?php


namespace Drupal\weta_tvss\Plugin\Action;


use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Resolve potential PBS Show duplicates.
 *
 * @Action(
 *   id = "weta_tvss_resolve_duplicates",
 *   label = @Translation("Resolve PBS Show duplicates"),
 *   type = "node"
 * )
 */
class ResolveDuplicate extends ActionBase {

  /**
   * @inheritDoc
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    /** @var \Drupal\node\NodeInterface $object */
    $result = $object->access('update', $account, TRUE)
      ->andIf($object->field_duplicate->access('edit', $account, TRUE));

    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * @param array $entities
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function executeMultiple(array $entities) {
    $count = count($entities);
    if ($count !== 2) {
      \Drupal::messenger()->addWarning(t('Please select two and only two shows to merge.'));
    }
    else {
      $tms_id = NULL;
      /** @var \Drupal\node\NodeInterface $entity */
      foreach ($entities as $entity) {
        if ($entity->hasField('field_tvss_updated_at') && $entity->hasField('field_updated_at')) {

          // If the updated at field is empty, this is a TVSS show.
          if (empty($entity->get('field_updated_at')->getValue())) {
            $tvss_entity = $entity;
          }
          // If the updated at field is not empty, this is a MM show.
          else {
            $mm_entity = $entity;
            // Get the TMS ID if it exists.
            $tms_id = $mm_entity->field_tms_id->value;

          }
        }
      }

      if (isset($tvss_entity)) {
        // If there is no TMS ID on the MM Show, get it from the TVSS Show.
        if (empty($tms_id)) {
          $tms_id = $tvss_entity->field_tms_id->value;
        }

        if (isset($mm_entity)) {
          $mm_entity_id = $mm_entity->id();

          // Get all airdates that reference the TVSS Show.  Set them to reference
          // the MM Show instead.
          $tvss_id = $tvss_entity->id();
          $query = \Drupal::entityQuery('airdate')
            ->condition('field_show', $tvss_id, '=')
            ->accessCheck(FALSE);
          $airdates = $query->execute();
          // Get an airdate storage object.
          $airdate_storage = \Drupal::entityTypeManager()->getStorage('airdate');
          $airdates = $airdate_storage->loadMultiple($airdates);

          foreach ($airdates as $airdate) {
            $airdate->field_show->target_id = $mm_entity_id;
            $airdate->save();
          }

          // Update the TMS ID, TVSS ID and the Potential
          // Duplicate fields on the MM Show.
          if ($mm_entity->hasField('field_duplicate')) {
            $mm_entity->field_duplicate->value = 0;
          }
          if ($mm_entity->hasField('field_tms_id')) {
            $mm_entity->field_tms_id->value = $tms_id;
          }
          if ($mm_entity->hasField('field_tvss_id')) {
            $mm_entity->field_tvss_id->value = $tvss_entity->field_tvss_id->value;
          }

          $mm_entity->save();


          // Delete the TVSS Show.
          $tvss_entity->delete();
        }


      }
    }
  }

  /**
   * @inheritDoc
   */
  public function execute($entity = NULL) {

  }

}
