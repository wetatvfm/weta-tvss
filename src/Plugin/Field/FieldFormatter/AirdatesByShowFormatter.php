<?php

namespace Drupal\weta_tvss\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\weta_tvss\AirdateManager;

/**
 * Plugin implementation of the 'Airdates by Show' formatter.
 *
 * @FieldFormatter(
 *   id = "Airdates by Show",
 *   label = @Translation("Display airdates"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class AirdatesByShowFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The EntityTypeManager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityManager;

  /**
   * The AirdateManager object.
   *
   * @var \Drupal\weta_tvss\AirdateManager
   */
  protected AirdateManager $airdateManager;

  /**
   * Constructs a new PlayerBlock object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager service.
   * @param \Drupal\weta_tvss\AirdateManager $airdate_manager
   *   The airdate manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_manager,
    AirdateManager $airdate_manager
  ) {
    parent::__construct($plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->entityManager = $entity_manager;
    $this->airdateManager = $airdate_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): AirdatesByShowFormatter|FormatterBase|ContainerFactoryPluginInterface|static {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('weta_tvss.airdate_manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Only show this formatter on the TVSS ID field on the Show content type.
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {

    $is_show = FALSE;
    $entity_type = $field_definition->getTargetEntityTypeId();
    $bundle = $field_definition->getTargetBundle();
    if ($entity_type == 'node' && $bundle == 'show') {
      $is_show = TRUE;
    }

    $is_tvss = FALSE;
    $field_name = $field_definition->getName();
    if ($field_name == 'field_tvss_id') {
      $is_tvss = TRUE;
    }

    return parent::isApplicable($field_definition) && $is_show && $is_tvss;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Display upcoming airdates for this show.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $view_builder = $this->entityManager->getViewBuilder('airdate');
    $schedule_items = [];

    foreach ($items as $item) {
      $tvss_id = $item->value;
      // Get all airdates with this TVSS ID starting from today.
      $airdates = $this->airdateManager->getAirdatesForShow($tvss_id);
      $count = 0;
      foreach ($airdates as $airdate) {
        // Only show the first 20 airdates to avoid triggering the
        // recursive rendering bug with the logo images.;
        if ($count < 20) {
          $schedule_items[] = [
            'value' => $view_builder->view($airdate, 'show'),
            '#wrapper_attributes' => [
              'class' => ['l-content-flow__item'],
            ],
          ];
        }
        $count++;
      }
    }

    return [
      '#theme' => 'item_list',
      '#items' => $schedule_items,
      '#attributes' => [
        'class' => ['l-content-flow', 'l-content-flow--divider'],
      ],
    ];
  }

}
