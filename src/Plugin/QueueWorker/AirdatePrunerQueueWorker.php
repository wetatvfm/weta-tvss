<?php

namespace Drupal\weta_tvss\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\weta_tvss\AirdateManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Removes old airdates from the database.
 *
 * Queue items are added by cron processing.
 *
 * @QueueWorker(
 *   id = "weta_tvss.queue.airdates_pruner",
 *   title = @Translation("TVSS Airdates Pruner"),
 *   cron = {"time" = 60}
 * )
 *
 * @see weta_tvss_cron()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class AirdatePrunerQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * WETA TVSS logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Airdates manager.
   *
   * @var \Drupal\weta_tvss\AirdateManager
   */
  private $airdateManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              LoggerChannelInterface $logger,
                              AirdateManager $airdate_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->airdateManager = $airdate_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): AirdatePrunerQueueWorker|ContainerFactoryPluginInterface|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.weta_tvss'),
      $container->get('weta_tvss.airdate_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $storage = $this->airdateManager->getStorage();
    $airdates = $storage->loadMultiple($data);
    $storage->delete($airdates);
  }

}

