<?php

namespace Drupal\weta_tvss\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\weta_tvss\ShowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Show data from the TVSS API to Show nodes.
 *
 * Queue items are added by cron processing.
 *
 * @QueueWorker(
 *   id = "weta_tvss.queue.shows",
 *   title = @Translation("TVSS Shows processor"),
 *   cron = {"time" = 60}
 * )
 *
 * @see weta_tvss_cron()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class ShowsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * WETA TVSS logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Show manager.
   *
   * @var \Drupal\weta_tvss\ShowManager
   */
  private ShowManager $showManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelInterface $logger,
    ShowManager $show_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->showManager = $show_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): ShowsQueueWorker|ContainerFactoryPluginInterface|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.weta_tvss'),
      $container->get('weta_tvss.show_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $this->showManager->addOrUpdateShow($data);
  }

}
