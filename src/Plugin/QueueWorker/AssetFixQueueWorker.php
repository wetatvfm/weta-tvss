<?php

namespace Drupal\weta_tvss\Plugin\QueueWorker;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Update any PBS Image media entity that meets conditions
 * set in the weta_tvss_post_update_fix_mismatched_images().
 *
 *
 * @QueueWorker(
 *   id = "weta_tvss.queue.images",
 *   title = @Translation("TVSS Image Fix processor"),
 *   cron = {"time" = 60}
 * )
 *
 * @see weta_tvss_post_update_fix_mismatched_images()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class AssetFixQueueWorker extends QueueWorkerBase implements
  ContainerFactoryPluginInterface {

  /**
   * WETA TVSS logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelInterface $logger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.weta_tvss')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {

    $media_storage =
      \Drupal::getContainer()->get('entity_type.manager')->getStorage('media');

    $affected_entities = $media_storage->loadMultiple($data);

    $count = 0;
    foreach ($affected_entities as $media) {
      // Get the remote image.
      try {
        $file_data = file_get_contents($media->field_remote_image->uri);
      } catch (\Exception $e) {
        if ($e->getMessage()) {
          $this->logger->error(t('There is no image at @url for Media @id.', [
            '@url' => $media->field_remote_image->uri,
            '@id' => $media->id()
          ]));
        }
      }


      // Create the directory from today's date as YYYY/MM/DD.
      $directory = 'public://pbs_tvss_images/' . date('Y/m/d');
      \Drupal::service('file_system')->prepareDirectory($directory,
        FileSystemInterface::CREATE_DIRECTORY);

      // Save the image.
      $file = \Drupal::service('file.repository')->writeData(
        $file_data,
        $directory . "/" . basename($media->field_remote_image->uri),
        FileSystemInterface::EXISTS_REPLACE
      );

      // Attach it to the media entity.
      if (is_object($file) && !empty($file->id())) {
        $media->set('field_media_image', [
          'target_id' => $file->id(),
          'alt' => $media->label(),
        ]);
      }
      $media->save();
      $count++;

    }
    $this->logger->notice(t('Fixed mismatched images for @count PBS Image entities. ', ['@count' =>
      $count]));
  }
}
