<?php

namespace Drupal\weta_tvss\Commands;

use Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\weta_tvss\AirdateManager;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 */
class TvssCommands extends DrushCommands {

  /**
   * The Airdates program manager.
   *
   * @var \Drupal\weta_tvss\AirdateManager
   */
  protected AirdateManager $airdateManager;


  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\weta_tvss\AirdateManager $airdate_manager
   *   The TVSS airdate manager.
   */
  public function __construct(AirdateManager $airdate_manager) {
    parent::__construct();
    $this->airdateManager = $airdate_manager;
  }

  /**
   * Command to force a full update of the schedule.
   *
   * @command weta_tvss:update
   * @aliases tvss-update, tvss:up
   */
  public function updateAll() {
    $request_time = Drupal::time()->getRequestTime();
    $now = new DrupalDateTime("@{$request_time}");

    // If notices have been disabled, temporarily enable them
    // so there's some output to prove this is running.
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = \Drupal::service('config.factory');
    $config = $config_factory->getEditable('weta_tvss.settings');
    $disable_notices = $config->get('api.disable_notices');

    $reset = FALSE;
    if ($disable_notices) {
      $config->set('api.disable_notices', FALSE);
      $config->save();
      $reset = TRUE;
    }

    $this->airdateManager->updateFromDate($now);
    $this->airdateManager->setLastFullUpdate(new DrupalDateTime());

    // If we enabled notices, disable them again.
    if ($reset) {
      $config->set('api.disable_notices', TRUE);
      $config->save();
    }
  }
}
