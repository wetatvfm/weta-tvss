<?php

namespace Drupal\weta_tvss;

use BadMethodCallException;
use Drupal\Core\Config\ConfigFactoryInterface;
use OpenPublicMedia\PbsTvSchedulesService\Client;


/**
 * API client class for the PBS TV Schedules Service (TVSS) API.
 *
 * @package Drupal\weta_tvss
 */
class ApiClient extends Client {

  /**
   * Config key for the API key.
   */
  const CONFIG_KEY = 'api.key';

  /**
   * Config key for the API call sign.
   */
  const CONFIG_CALL_SIGN = 'api.call_sign';

  /**
   * API key.
   *
   * @var string
   */
  private $key;

  /**
   * API call sign.
   *
   * @var string
   */
  public string $callSign;

  /**
   * API base endpoint.
   *
   * @var string
   */
  public $baseUri;

  /**
   * WETA TVSS immutable config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ApiClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('weta_tvss.settings');
    $this->key = $this->config->get(self::CONFIG_KEY);
    $this->callSign = $this->config->get(self::CONFIG_CALL_SIGN);
    parent::__construct($this->key, $this->callSign);
  }

  /**
   * Gets the API client key.
   *
   * @return string|null
   *   TVSS API key setting or NULL if none is set.
   */
  public function getApiKey(): ?string {
    return $this->key;
  }

  /**
   * Gets the API client call sign.
   *
   * @return string|null
   *   TVSS API call sign setting or NULL if none is set.
   */
  public function getCallSign(): ?string {
    return $this->callSign;
  }

  /**
   * Sends a test query to the API and returns an error or "OK".
   *
   * @return string
   *   An error string or "OK" (indicating that the connection is good).
   */
  public function testConnection(): string {
    $result = 'OK';

    try {
      $this->getToday();
    }
    catch (BadMethodCallException $e) {
      $error = $e->getMessage();
      if (isset($error->detail)) {
        $result = $error->detail;
      }
      else {
        $result = (string) $error;
      }
    }

    return $result;
  }

}
