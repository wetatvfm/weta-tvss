<?php

namespace Drupal\weta_tvss;

use DateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\FileInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Exception;

/**
 * Class ApiContentManagerBase.
 */
abstract class ApiContentManagerBase implements ApiContentManagerInterface {
  use StringTranslationTrait;

  /**
   * PBS TVSS API client wrapper.
   *
   * @var \Drupal\weta_tvss\ApiClient
   *
   * @see \OpenPublicMedia\PbsTvSchedulesService\Client
   */
  protected ApiClient $client;

  /**
   * The Drupal field name for TVSS API item CIDs.
   */
  const CID_FIELD_NAME = 'field_cid';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The TVSS settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The PBS Media Manager settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $mediaManagerConfig;

  /**
   * Media Manager logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * ApiContentManagerBase constructor.
   *
   * @param \Drupal\weta_tvss\ApiClient $client
   *   Media Manager API client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The filesystem service.
   */
  public function __construct(
    ApiClient $client,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
    QueueFactory $queue_factory,
    StateInterface $state,
    FileSystemInterface $file_system
  ) {
    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('weta_tvss.settings');
    $this->logger = $logger;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueue(): QueueInterface {
    return $this->queueFactory->get($this->getQueueName());
  }

  /**
   * {@inheritdoc}
   */
  public function getApiClient(): ApiClient {
    return $this->client;
  }

  /**
   * Gets the storage for the local content type.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Storage interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage($this->getEntityTypeId());
  }

  /**
   * Gets the definition for the local content type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   Entity type interface or NULL if not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDefinition(): ?EntityTypeInterface {
    return $this->entityTypeManager->getDefinition($this->getEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function getContentByCid(string $cid): ?ContentEntityInterface {
    try {
      $items = $this->getContentByProperties([self::CID_FIELD_NAME => $cid]);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $items = [];
    }

    $item = NULL;
    if (!empty($items)) {
      $item = reset($items);
      if (count($items) > 1) {
        $this->logger->error($this->t('Multiple items found for TVSS CID @cid. @label IDs found: @id_list. Using @id.', [
          '@cid' => $cid,
          '@label' => $item->getEntityType()->getLabel(),
          '@id_list' => implode(', ', array_keys($items)),
          '@id' => $item->id(),
        ]));
      }
    }

    return $item;
  }

  /**
   * Gets content using provided properties.
   *
   * @param array $properties
   *   Conditions used to query for content.
   * @param string|null $sort_by
   *   Field/property to sort by.
   * @param string $sort_dir
   *   Sort direction (should be "ASC" or "DESC").
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Content matching the properties.
   */
  public function getContentByProperties(
    array $properties,
    string $sort_by = NULL,
    string $sort_dir = 'ASC'
  ): array {

    try {
      $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage($this->getEntityTypeId());
    }
    catch (Exception $e) {
      $this->logger->critical($this->t('Invalid entity configuration for API
        content detected. Unable to query content.'));
      watchdog_exception('weta_tvss', $e);
      return [];
    }

    $query = $storage->getQuery();
    $query->accessCheck(FALSE);
    if ($this->getBundleId()) {
      $query->condition($definition->getKey('bundle'), $this->getBundleId());
    }
    foreach ($properties as $property => $value) {
      $query->condition($property, $value);
    }
    if (!empty($sort_by)) {
      $query->sort($sort_by, $sort_dir);
    }

    $ids = $query->execute();
    $storage->loadMultiple($ids);

    return $storage->loadMultiple($ids);
  }

  /**
   * Gets or creates a node based on API data.
   *
   * @param string $guid
   *   Media Manager GUID of the object to get a Node for.
   * @param string $bundle
   *   The bundle type of the item being retrieved.
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return \Drupal\node\NodeInterface
   *   The node to use.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getOrCreateNode(string $guid, string $bundle, string $resource): NodeInterface {
    $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    $node = $this->getNodeByGuid($guid, $bundle, $resource);

    if (empty($node)) {
      $node = Node::create([
        $definition->getKey('bundle') => $bundle,
      ]);
      if ($uid = $this->config->get('api.queue_user')) {
        $node->setOwnerId($uid);
      }
      $node->enforceIsNew();
    }

    return $node;
  }

  /**
   * Attempts to get Drupal guid field for a resource.
   *
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return string|null
   *   The Drupal ID field of the Media Manager resource or NULL.
   */
  public function getNodeGuidField(string $resource): ?string {
    return $this->mediaManagerConfig->get($resource . '.mappings.id');
  }

  /**
   * Attempts to get Drupal updated at field for a resource.
   *
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return string|null
   *   The Drupal ID field of the Media Manager resource or NULL.
   */
  public function getNodeUpdatedField(string $resource): ?string {
    return $this->config->get($resource . '.mappings.updated_at');
  }

  /**
   * Attempts to get a local node by a Media Manager ID.
   *
   * @param string $guid
   *   Media Manager GUID of the object to get a Node for.
   * @param string $bundle
   *   The bundle type of the item being retrieved.
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Related node or NULL if none found.
   */
  public function getNodeByGuid(string $guid, string $bundle, string $resource): ?NodeInterface {
    $guid_field = $this->getNodeGuidField($resource);
    try {
      $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage($this->getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $bundle,
        $guid_field => $guid,
      ]);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    $node = NULL;
    if (!empty($nodes)) {
      $node = reset($nodes);
      if (count($nodes) > 1) {
        $this->logger->error('Multiple nodes found for Media Manager
          ID {id}. Node IDs found: {nid_list}. Updating node {nid}.', [
            'id' => $guid,
            'nid_list' => implode(', ', array_keys($nodes)),
            'nid' => $node->id(),
          ]);
      }
    }

    return $node;
  }

  /**
   * Returns the ID field from a node or NULL if empty.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to evaluate.
   *
   * @return string|null
   *   ID field value or NULL.
   */
  public static function getNodeGuid(NodeInterface $node): ?string {
    try {
      /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
      $config_factory = \Drupal::service('config.factory');
      $config = $config_factory->get('pbs_media_manager.settings');
      $id_field =  $config->get('shows.mappings.id');
      return $node->get($id_field)->value;
    }
    catch (Exception $e) {
      return NULL;
    }
  }

  /**
   * Indicates if a node has a non-empty ID value.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to evaluate.
   *
   * @return bool
   *   TRUE if the node has a non-empty ID field, FALSE otherwise.
   */
  public static function nodeHasGuid(NodeInterface $node): bool {
    return !empty(self::getNodeGuid($node));
  }

  /**
   * Gets a DateTime object without microsecond precision.
   *
   * Media Manager provides microseconds in the `updated_at` field for objects,
   * but Drupal's storage does not record them. This will check for microseconds
   * in a Media Manager date string and remove them if they exist before
   * creating a DateTime object from the string.
   *
   * @param string $datetime
   *   A string representation of datetime in the Media Manager format, either
   *   Y-m-d\TH:i:s\Z or Y-m-d\TH:i:s.u\Z.
   *
   * @return \DateTime|null
   *   DateTime object without microseconds or NULL if creation fails.
   */
  public static function dateTimeNoMicroseconds(string $datetime): ?DateTime {
    // If the format is Y-m-d\TH:i:s.u\Z, the length will be 27 characters and
    // the last eight should be striped to remove microseconds.
    if (strlen($datetime) == 27) {
      $datetime = substr($datetime, 0, -8) . 'Z';
    }
    try {
      $object = new DateTime($datetime);
    }
    catch (Exception $e) {
      $object = NULL;
    }
    return $object;
  }

  /**
   * Convert images array to key by profile and enforce site scheme.
   *
   * This is necessary because some images provided by Media Manager use an
   * "http" scheme. This will cause mixed media errors and prevent images from
   * loading because the website uses an "https" scheme.
   *
   * @param array $images
   *   Images from a Media Manager query.
   * @param string $image_key
   *   Images array key containing the image URL.
   * @param string $profile_key
   *   Images array key containing the image profile string.
   *
   * @return array
   *   All valid images keyed by profile string using a "\\" scheme to match the
   *   site scheme.
   */
  public function parseImages(
    array $images,
    string $image_key = 'image',
    string $profile_key = 'external_profile'
  ): array {
    $images = array_column($images, $image_key, $profile_key);
    foreach ($images as $key => $image) {
      $parts = parse_url($image);
      if ($parts === FALSE || !isset($parts['host']) || !isset($parts['path'])) {
        unset($images[$key]);
      }
      else {
        $images[$key] = sprintf('//%s%s', $parts['host'], $parts['path']);
      }
    }
    return $images;
  }

  /**
   * Gets latest `updated_at` field from an API response object.
   *
   * This method accounts for the `updated_at` fields in the images array
   * for a item existing, when they don't for the item generally.
   * @param object $item
   *   API object.
   *
   * @return \DateTime|null
   *   Latest `updated_at` field value or NULL if DateTime create fails.
   */
  public static function getLatestUpdatedAt(object $item): ?DateTime {
    $request_time = \Drupal::time()->getRequestTime();
    $updated_at = new DateTime("@{$request_time}");


    if (isset($item->images)) {
      foreach ($item->images as $image) {
        $image_updated_at = self::dateTimeNoMicroseconds($image->updated_at);
        if ($image_updated_at > $updated_at) {
          $updated_at = $image_updated_at;
        }
      }
    }

    return $updated_at;
  }

  /**
   * Creates a image media item based on the configured field values.
   *
   * @param object $image
   *   Image from TVSS, including profile, image (URL), image type, and
   * updated_at.
   * @param string $image_title
   *   A title for the image.
   * @param string $type
   *   The resource type, such as "show" or "episode".
   * @param object|null $media_image
   *   The existing image, if updating.
   *
   * @return string|null
   *   A Drupal media image id or null.
   */
  protected function addOrUpdateMediaImage(object $image, string $image_title, string $type, ?object $media_image): ?string {

    // Get required configuration.
    $image_media_type = $this->config->get($type . 's.image_media_type');
    if (empty($image_media_type)) {
      $this->logger->error('Please configure the media image type used on ' .
        $type . 's.');
      return NULL;
    }
    $image_mapping = $type . 's.' . $type . '_field_mappings.tvss_image';
    if ($type == 'show') {
      $image_mapping = $type . 's.mappings.tvss_only.tvss_image';
    }
    $image_field = $this->config->get($image_mapping);
    if (empty($image_field)) {
      $this->logger->error('Please configure the ' . $type . ' image field.');
      return NULL;
    }
    $image_field_mappings =
      $this->config->get($type . 's.image_field_mappings');
    $image_title_field = $image_field_mappings['image_title'];
    $image_image_field = $image_field_mappings['image_field'];
    $image_remote_field = $image_field_mappings['image_remote'];
    $image_updated_field = $image_field_mappings['image_updated'];
    $image_type_field = $image_field_mappings['image_type'];

    if ($image_title_field == 'unused' || $image_image_field == 'unused' ||
      $image_updated_field == 'unused' || $image_remote_field == 'unused' ||
      $image_type_field == 'unused') {
      $this->logger->error('Please configure all 4 fields for ' . $type .
        ' images.');
      return NULL;
    }

    // Set up media storage.
    $media_storage = $this->entityTypeManager->getStorage('media');

    // Set the caption as the alt text, if it exists. Otherwise use the title.
    $alt = $image_title;
    if ($image->caption) {
      $alt = $image->caption;
    }

    // Reformat the updated at field.
    $updated_dt = self::dateTimeNoMicroseconds($image->updated_at);

    // Determines if the file needs to be downloaded and saved.
    $add_file = FALSE;

    // If a media image already exists for this item.
    if ($media_image && $drupal_updated_str =
        $media_image->{$image_updated_field}->value) {
      $drupal_updated_dt = self::dateTimeNoMicroseconds($drupal_updated_str);

      if ($updated_dt->getTimestamp() > $drupal_updated_dt->getTimestamp()) {

        // If the referenced image has changed, determine if the new image
        // has already been imported.
        if ($image->image !== $media_image->{$image_remote_field}->uri) {
          // If a media image already exists in Drupal with the remote
          // source, use that instead.
          if ($media_id = $this->getImageByRemoteSource($image->image,
            $image_remote_field, $image_title)) {
            $media_image = $media_storage->load($media_id);
          }
          // If a media image does NOT already exist, we will need to
          // create a new one.
          else {
            $media_image = NULL;
          }
        }

        // If we still have an image that we're working with, update any
        // metadata fields.
        if ($media_image) {
          $media_image->set($image_updated_field, $updated_dt->format
          (DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
          $media_image->set($image_title_field, $image_title);
          $media_image->set($image_type_field, $image->type);
          $media_image->save();
        }
      }
    }


    // If the media entity does not exist on the airdate, find or create one.
    // NOTE: We cannot simply say else because we may have removed the media
    // image above.
    if (!$media_image) {

      // Check to see if a media image already exists in Drupal with the
      // remote source.
      if ($media_id = $this->getImageByRemoteSource($image->image,
        $image_remote_field, $image_title)) {

        // If so, load it.
        $media_image = $media_storage->load($media_id);

        // If the type field is empty, update that and the title.
        if ($media_image->{$image_type_field}->isEmpty()) {
          $media_image->set($image_type_field, $image->type);
          $media_image->set($image_title_field, $image_title);
          $media_image->save();
        }
      }

      // If a media image cannot be found, create a new one.
      else {
        // Create a media entity.
        $media_image = $media_storage->create([
          $image_title_field => $image_title,
          'bundle' => $image_media_type,
          'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
          $image_updated_field => $updated_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
          $image_remote_field => $image->image,
          $image_type_field => $image->type
        ]);
        // Set the owner, if configured.
        if ($uid = $this->config->get('api.queue_user')) {
          $media_image->setOwnerId($uid);
        }
        $add_file = TRUE;
      }
    }

    // Update the image file, if necessary.
    if ($add_file) {
      // Attach the image file.
      $file = $this->saveImagefile($image);
      if (is_object($file) && !empty($file->id())) {
        $media_image->set($image_image_field, [
          'target_id' => $file->id(),
          'alt' => $alt,
        ]);
      }
      $media_image->save();
    }

    return $media_image->id();
  }

  /**
   * Load an image by its remote source.
   *
   * @param string $source
   *   The remote source.
   * @param string $image_remote_field
   *   The remote image field to query.
   * @param string $image_title
   *   The image title.
   *
   * @return string|null
   *   A Drupal media image id or null.
   */
  protected function getImageByRemoteSource(string $source, string
  $image_remote_field, string $image_title): ?string {

    $media_ids = \Drupal::entityQuery('media')
      ->condition('bundle', 'pbs_image')
      ->condition($image_remote_field, $source, '=')
      ->accessCheck(FALSE)
      ->execute();

    // Ideally, there wil only be one match. If not, pick the first result,
    // generate a notice and merge the duplicates.
    if (!empty($media_ids)) {
      sort($media_ids);
      $media_id = array_shift($media_ids);
      if (count($media_ids) >= 1) {
        $this->logger->notice($this->t('Multiple items found for Media Image @title. Using @id. Merging Media IDs: @id_list. ',
          [
            '@title' => $image_title,
            '@id_list' => implode(', ', $media_ids),
            '@id' => $media_id,
          ]));
        $this->mergeImages($media_id, $media_ids);
      }
      return $media_id;
    }
    return NULL;
  }

  /**
   * Creates a Drupal image file.
   *
   * @param object $image
   *   Image from Media Manager, including profile, image (URL), and updated_at.
   *
   * @return \Drupal\file\FileInterface|false
   *   A file entity, or FALSE on error.
   */
  protected function saveImagefile(object $image): bool|FileInterface {

    // Create the directory from today's date as YYYY/MM/DD.
    $directory = 'public://pbs_tvss_images/' . date('Y/m/d');
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    // Download the file.
    try {
      $file_data = file_get_contents($image->image);
    }
    catch (\Exception $e) {
      if ($e->getMessage()) {
        $this->logger->error($this->t('There is no image at @url.', [
          '@url' => $image->image,
        ]));
      }
      return FALSE;
    }

    // Save the image.
    $file = \Drupal::service('file.repository')->writeData(
      $file_data,
      $directory . "/" . basename($image->image),
      FileSystemInterface::EXISTS_REPLACE
    );

    return $file;
  }


  /**
   * Merge duplicate media entities.
   *
   * @param string $media_id
   *   The id of the primary media entity we plan to keep.
   * @param array $media_ids
   *   Media ids for duplicate entities that we plan to merge and delete.
   *
   * @return void
   */
  protected function mergeImages(string $media_id, array $media_ids): void {
    foreach ($media_ids as $dupe) {
      // Get all airdates that reference the duplicate image.  Set them to
      // reference the primary image instead.
      $query = \Drupal::entityQuery('airdate')
        ->condition('field_tvss_image', $dupe, '=')
        ->accessCheck(FALSE);
      $airdates = $query->execute();
      // Get an airdate storage object.
      $airdate_storage = \Drupal::entityTypeManager()->getStorage('airdate');
      $airdates = $airdate_storage->loadMultiple($airdates);

      foreach ($airdates as $airdate) {
        $airdate->field_tvss_image->target_id = $media_id;
        $airdate->save();
      }

      // Get all shows that reference the duplicate image. Set them to
      // reference the primary image instead.
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'show')
        ->condition('field_tvss_image', $dupe, '=');
      $shows = $query->execute();
      // Get an airdate storage object.
      $show_storage = \Drupal::entityTypeManager()->getStorage('node');
      $shows = $show_storage->loadMultiple($shows);

      foreach ($shows as $show) {
        $show->field_tvss_image->target_id = $media_id;
        $show->save();
      }

      // Delete the duplicate entity.
      $media_storage = \Drupal::entityTypeManager()->getStorage('media');
      $dupe_entity = $media_storage->load($dupe);
      $dupe_entity->delete();
    }
  }


  /**
   * Determines what operation to perform on a Drupal entity based on API data.
   *
   * @param object $item
   *   Media Manager API item (shows, season, episode, or asset).
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Drupal entity (shows, season, episode, or asset).
   * @param array $mappings
   *   The field mappings for this resource.
   * @param bool $force
   *   Whether to "force" the update.
   *
   * @return string
   *   The operation to be performed: 'created', 'updated', 'skipped', or
   *   'configured'. Other modules can set it to 'ignored'.
   */
  protected function getOperation(object $item, EntityInterface $entity, array $mappings, bool $force): string {

    // Default to skipped. This should only be unchanged resources.
    $operation = 'skipped';

    // Confirm that the title, id, and updated_at fields are mapped.
    $title_field = $mappings['shared']['title'];
    $id_field = $mappings['tvss_only']['tvss_id'];
    $updated_at_field = $mappings['tvss_only']['updated_at'];
    $drupal_updated_at = $entity->get($updated_at_field)->value;
    if (empty($title_field) || $title_field == 'unused' ||
      empty($id_field) || $id_field == 'unused' ||
      empty($updated_at_field) || $updated_at_field == 'unused') {
      $operation = 'configured';
    }
    // Always return 'created' for new entities.
    elseif ($entity->IsNew()) {
      $operation = 'created';
    }
    // If 'force' is requested and it is not a new entity, then the entity needs
    // to be updated.
    elseif ($force) {
      $operation = 'updated';
    }
    // Always return 'updated' if the updated_at field on the entity is empty.
    elseif (empty($drupal_updated_at)) {
      $operation = 'updated';
    }
    // For existing Drupal entities, check if the item has changed in PBS.
    elseif ($pbs_updated_ts = self::getLatestUpdatedAt($item)->getTimestamp()) {
      // Get the updated_at time of the Drupal entity in UTC time.
      $dt_updated = new DrupalDateTime($drupal_updated_at, DateTimeItemInterface::STORAGE_TIMEZONE);
      $drupal_updated_ts = $dt_updated->getTimestamp();
      // If the updated time in PBS is greater than the updated time of the
      // Drupal entity, then the entity needs to be updated.
      if ($pbs_updated_ts > $drupal_updated_ts) {
        $operation = 'updated';
      }
    }

    // Allow other modules to alter the operation (e.g. change it to 'ignored').
    $context = [
      'item' => $item,
      'entity' => $entity,
    ];
    $this->moduleHandler->alter('weta_tvss_operation', $operation, $context);

    return $operation;
  }

  /**
   * Jumping through hoops to create a unique identifier
   * that spans shows and programs since there is none in
   * V2 of the API
   *
   * @param $listing object
   *
   * @return string
   */
  public function getIndex(object $listing): string {
    $id = '';
    if (isset($listing->program_id) && $listing->program_id !== NULL) {
      $id = $listing->program_id;
    }
    elseif (isset($listing->show_id) && $listing->show_id !== NULL) {
      $id = $listing->show_id;
    }

    return $id;
  }

}
