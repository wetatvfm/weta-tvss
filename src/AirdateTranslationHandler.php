<?php

namespace Drupal\weta_tvss;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for airdate.
 */
class AirdateTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
