<?php

namespace Drupal\weta_tvss;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Airdate entity.
 *
 * @see \Drupal\weta_tvss\Entity\Airdate.
 */
class AirdateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\weta_tvss\Entity\AirdateInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished airdate entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published airdate entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit airdate entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete airdate entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add airdate entities');
  }


}
