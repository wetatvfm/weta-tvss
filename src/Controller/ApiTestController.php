<?php

namespace Drupal\weta_tvss\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\weta_tvss\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test controller for the pbs_media_manager module.
 */
class ApiTestController extends ControllerBase {

  /**
   * The PBS Media Manager client.
   *
   * @var \Drupal\weta_tvss\ApiClient
   */
  protected ApiClient $client;

  /**
   * Constructs the ApiTestController.
   *
   * @param \Drupal\weta_tvss\ApiClient $api_client
   *   The Media Manager API service.
   */
  public function __construct(ApiClient $api_client) {
    $this->client = $api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ApiTestController|static {
    return new static(
      $container->get('weta_tvss.api_client')
    );
  }

  /**
   * Sends a test query to the API and returns result.
   *
   * @throws \Exception
   */
  public function testConnection(): array {

    $response = $this->client->testConnection();

    $message = $response;
    if ($response == "OK") {
      $message = 'The API test succeeded.';
    }

    return [
      '#theme' => 'item_list',
      '#title' => 'Test Result',
      '#items' => [$message],
    ];
  }

}
