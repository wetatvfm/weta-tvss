<?php
/**
 * @file
 * weta_tvss.post_update.php
 */

/**
 * Fix issues where the stored file no longer matches the remote image on PBS.
 */
function weta_tvss_post_update_fix_mismatched_images(&$sandbox) {

  if (!isset($sandbox['progress'])) {

    //Get all the PBS Image entities where the name includes TVSS.
    $media_storage =
      Drupal::getContainer()->get('entity_type.manager')->getStorage('media');
    $media_ids = $media_storage->getQuery();
    $media_ids = $media_ids->condition('bundle', 'pbs_image')
      ->condition('name', 'TVSS', 'CONTAINS')
      ->condition('field_media_image', '', '<>')
      ->accessCheck(FALSE)
      ->execute();

    $affected_entities = [];

    foreach ($media_ids as $mid) {
      // Load the entity.
      $media = $media_storage->load($mid);

      $filename = NULL;
      // Get the referenced files.
      if ($image_references = $media->field_media_image) {
        foreach ($image_references as $image_reference) {
          $file_id = $image_reference->get('target_id')->getValue();
          if ($referenced_file = Drupal::getContainer()
            ->get('entity_type.manager')
            ->getStorage('file')
            ->load($file_id)) {
            // Get the filename.
            $filename = $referenced_file->get('filename')->value;
          }
        }
      }

      $remote_filename = NULL;
      // Get the filename for the remote image.
      if ($remote_image = $media->field_remote_image->uri) {
        $remote_filename = basename($remote_image);
      }

      if (!empty($filename) && !empty($remote_filename) && $filename !==
        $remote_filename) {
        $affected_entities[] = $media;
      }
    }


    // 'max' is the number of total records we’ll be processing.
    $sandbox['max'] = count($affected_entities);
    // If 'max' is empty, we have nothing to process.
    if (empty($sandbox['max'])) {
      $sandbox['#finished'] = 1;
      return;
    }

    // 'progress' will represent the current progress of our processing.
    $sandbox['progress'] = 0;

    // 'entities_per_batch' is a custom amount that we’ll use to limit
    // how many entities we’re processing in each batch.
    // This is a large part of how we limit expensive batch operations.
    $sandbox['entities_per_batch'] = 20;

    // 'media_ids' will store the entity IDs of the PBS Video entities
    // that we just queried for above during this initialization phase.
    $sandbox['affected_entities'] = $affected_entities;
  }

  // Calculates current batch range.
  $range_end = $sandbox['progress'] + $sandbox['entities_per_batch'];
  if ($range_end > $sandbox['max']) {
    $range_end = $sandbox['max'];
  }

  $batch = [];
  // Loop over current batch range, updating the media entities.
  for ($i = $sandbox['progress']; $i < $range_end; $i++) {
    $batch[] = $sandbox['affected_entities'][$i]->id();
  }

  // Add them to a queue to be processed on cron runs later.
  /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
  $queue_factory = \Drupal::service('queue');

  $queue = $queue_factory->get('weta_tvss.queue.images');

  $queue->createItem($batch);

  // Update the batch variables to track our progress

  // We can calculate our current progress via a mathematical fraction.
  // Drupal’s Batch API will stop executing our update hook as soon as
  // $sandbox['#finished'] == 1 (viz., it evaluates to TRUE).
  $sandbox['progress'] = $range_end;
  $progress_fraction = $sandbox['progress'] / $sandbox['max'];
  $sandbox['#finished'] = empty($sandbox['max']) ? 1 : $progress_fraction;

}
